import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router'
import {setLanguage} from "redux-i18n"

class Layout extends Component {

    componentWillMount() {
        this.props.setLanguage("en")
    }

    render() {
        const { children, location } = this.props
        const {t} = this.context
        return (
            <div>
                <Menu>
                    <Menu.Item
                        as={Link}
                        to='/messages'
                        icon='home'/>
                    <Menu.Item
                        as={Link}
                        to='/subtopics'
                        active={location.pathname === '/subtopics'}>
                       {t('Subtópicos')}
                    </Menu.Item>
                    <Menu.Item
                        as={Link}
                        to='/items'
                        active={location.pathname === '/items'}>
                       {t('Itens')}
                    </Menu.Item>
                    <Menu.Item
                    as={Link}
                    to='/messages'
                    active={location.pathname === '/messages'}>
                        {t('Mensagens')}
                    </Menu.Item>
                </Menu>
                {children}
            </div>
        )
    }
}

Layout.propTypes = {
    location: React.PropTypes.shape({
        pathname: React.PropTypes.string.isRequired,
    }).isRequired,
    params: React.PropTypes.shape({
        type: React.PropTypes.string,
    }).isRequired,
    children: React.PropTypes.element,
}

Layout.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default connect(
    null, 
    (dispatch) => ({
        setLanguage: (l) => dispatch(setLanguage(l))
    })
)(Layout)