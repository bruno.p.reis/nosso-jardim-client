import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const AREAS_QUERY = gql`
query areas {
  areas {
    id
    name
    items {
      id
      name
    }
  }
}

`;

const withAreasQuery = graphql(
	AREAS_QUERY,
	{
		props:({data:{areas}})=>{
			return {
				areas:areas
			}
		}
	}
);
export default withAreasQuery;