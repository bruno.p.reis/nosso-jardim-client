import React, { Component } from 'react'
import LoaderAndError from '../components/LoaderAndError'

/**
* HOC - wrap your component and get this props:
* method runMutation - wrap your promise call (mutate) to it to have mutationLoaderAndError injected for you
* mutationLoaderAndError - will show loading status and error messages
* limitations: if you call your mutation twice, this might have unexpected results because one might start before the other one finishes
*/
export default function mutationRunner(WrappedComponent) {

    return class extends Component {

    	constructor(props) {
    		super(props)
    		this.state = {
    			mutationRunning: false,
    			mutationError: null
    		}
    	}

        runMutation = (action,successCallback) => {
        	this.setState({mutationRunning:true});
            return action().then(()=>{
                this.setState({ mutationRunning: false });
                if(successCallback) {
                    successCallback()
                }
            }).catch((error) =>{
                this.setState({ 
                    mutationRunning: false, 
                    mutationError: error.message ? error.message : error
                });
            })
        }

        render() {
        	let dismissFunc = ()=>{
        		this.setState({mutationError:false})
        	}
            let mutationLoaderAndError = <LoaderAndError 
                dismissFunc={dismissFunc}
                loading={this.state.mutationRunning} 
                errorMessage={this.state.mutationError}/>
        	return (
    				<WrappedComponent
                        runMutation={this.runMutation}
    					mutationLoaderAndError={mutationLoaderAndError}
    					{...this.props}/>
        	)
        }
    }
}
