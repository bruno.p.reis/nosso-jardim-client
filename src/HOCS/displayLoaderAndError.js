import React from 'react';
import LoaderAndError from '../components/LoaderAndError'
import { branch, renderComponent } from 'recompose';

const LoaderAndErrorC = (props) => {
    const {loading,error} = props
    return <LoaderAndError loading={loading} errorMessage={error}/>
}

export default branch(
    ({loading,error})=> loading || error,
    renderComponent(LoaderAndErrorC)
)