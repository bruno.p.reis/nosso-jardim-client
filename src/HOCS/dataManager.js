import React, { Component } from 'react'
import _ from 'lodash'
import update from 'immutability-helper';

/**
* HOC - wrap your component and get your data managed - used for forms that: 
*   - keep original data and 
*   - are controlled by the state of the new data being set
* adds four props:
* data - form data
* changeData(dataChange) - receives an array with new data. Like: {name:"Dude",age:38}
* dataHasChanged - true if data is different from initial data
* resetData() - reset the form to initial data
*/
export default function dataManager(config) {
    
    const propList = _.keys(config.dataHash);
    let originData = null;

    function wrap(WrappedComponent) {
        return class extends Component {

        	constructor(props) {
                super(props);
                let initialData = Object.assign( {}, config.dataHash )
                if(config.initialDataProp && props[config.initialDataProp]) {
                    let origin = props[config.initialDataProp];
                    this.populateInitialValues(initialData,origin)
                }
                originData = initialData
                this.state = {data:initialData}
            }

            populateInitialValues(initialData,origin) {
                _.each(propList,(v)=>{
                    if(origin[v] !== undefined) {
                        initialData[v] = origin[v];
                    }
                })
            }

            populatePropValues(origin) {
                let initialData = {}
                _.each(propList,(v)=>{
                    if(origin[v] !== undefined) {
                        initialData[v] = origin[v]
                    }
                })
                originData = initialData
                this.changeData(initialData)
            }

            componentWillReceiveProps(nextProps) {
                if(config.initialDataProp && nextProps[config.initialDataProp]) {
                    this.populatePropValues(nextProps[config.initialDataProp])
                }
            }

            resetData = () => {
                this.changeData(originData)
            }

            changeData = (newData) => {
                const edit = {};
                _.each(
                    propList,
                    (k)=>{
                        if( newData[k] !== undefined ) {
                            edit[k] = {$set:newData[k]}
                        }
                    }
                )
                this.setState({
                    data: update(this.state.data,edit)
                })
            }

            // this will probably need some improvement
            // originalData will need to consider an ObjectAssign({},config.dataHash,initialData)
            dataHasChanged = () => {
                return !_.isEqual(this.state.data,originData);
            }

            render() {
            	return (
        				<WrappedComponent
                            changeData={this.changeData}
                            data={this.state.data}
                            resetData={this.resetData}
                            dataHasChanged={this.dataHasChanged()}
        					{...this.props}/>
            	)
            }
        }
    }
    return wrap;
}
