import React, {Component} from 'react';
import {Grid} from 'semantic-ui-react'
import SubtopicsChooserContainer from './components/SubtopicsChooserContainer'
import SubtopicPanelContainer from './components/SubtopicPanelContainer'

class ItemsPage extends Component {
    render() {
        const {
            route:{createItem,editItem},
            params:{itemId,subtopicId}
        } = this.props
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column width='4'>
                        <SubtopicsChooserContainer 
                            selectedSubtopicId={this.props.params.subtopicId}/>
                    </Grid.Column>
                    <Grid.Column width='12'>
                        <SubtopicPanelContainer 
                            createItem={createItem}
                            editItem={editItem}
                            itemId={itemId}
                            subtopicId={subtopicId}/>
                    </Grid.Column >
                </Grid.Row>
            </Grid>
        )
    }
}

export default ItemsPage;
