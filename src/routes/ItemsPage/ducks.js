import { browserHistory } from 'react-router'

export function navSubtopic(subtopicId) {
	let path = '/items/subtopic/' + subtopicId
    browserHistory.push(path)
}

export function navCreateItem(subtopicId) {
	let path = '/items/subtopic/' + subtopicId + '/createItem'
    browserHistory.push(path)
}

export function navEditItem(subtopicId,itemId) {
	let path = '/items/subtopic/' + subtopicId + '/item/' + itemId + '/edit'
    browserHistory.push(path)
}