import { graphql } from 'react-apollo';
import REGISTER_MUTATION from '../mutations/register.graphql';
import ITEMS_QUERY from '../queries/Items.graphql';
import {compose} from 'react-apollo';
import _ from 'lodash'
import mutationRunner from '../../../HOCS/mutationRunner'
import dataManager from '../../../HOCS/dataManager'
import CreateItemForm from '../components/CreateItemForm'

const withData = graphql(REGISTER_MUTATION,{
    props: ({mutate, ownProps}) => ({
        submit: (data) => {
        	let variables = {
            	subtopicId:ownProps.subtopicId, 
            	...data
            }
            return  mutate({
            	variables,
			    refetchQueries: [{
					query: ITEMS_QUERY,
					variables: {subtopicId:ownProps.subtopicId},
				}],
            })
        }
    }),
})

export default compose(
    withData,
	mutationRunner,
	dataManager({
		dataHash:{name:''}
	})
)(CreateItemForm);
