import React from 'react';
import {List,Grid,Header, Segment, Button} from 'semantic-ui-react'
import ItemListItemContainer from "./ItemListItemContainer"
import CreateItemFormContainer from './CreateItemFormContainer'
import EditItemFormContainer from './EditItemFormContainer'

const SubtopicItemsList = ({ 
    subtopic,
    items,
    navCreateItem,
    navEditItem,
    navSubtopic,
    createItem,
    editItem,
    refetchItems,
    itemId
}) => {
    if(subtopic) {
        let listItems = [];
        if(items) {
            listItems = items.map((item, i) =>
                <ItemListItemContainer 
                    item={item} 
                    key={i} 
                    edit={()=>{navEditItem(subtopic.id,item.id)}}
                    afterDelete={refetchItems}/>
            );    
        }
        return (
            <span>
                <Header content={subtopic.name} as="h3" style={{paddingBottom:'20px'}}/>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width='9'>
                            <Segment>
                                <Grid style={{paddingBottom:'20px'}}>
                                    <Grid.Column width='8'>
                                        <Header as='h2'>
                                            Items
                                        </Header>
                                    </Grid.Column>
                                    <Grid.Column width='8'>
                                        <Button
                                            size='mini'
                                            primary
                                            floated='right'
                                            content='Adicionar Item'
                                            icon='add circle'
                                            labelPosition='right'
                                            onClick={navCreateItem}/>
                                    </Grid.Column>
                                </Grid>
                                <List divided relaxed verticalAlign='middle'>
                                    {listItems}
                                </List>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width='5' verticalAlign='top'>
                            {
                                createItem && subtopic
                                ? 
                                <CreateItemFormContainer
                                    close={()=>navSubtopic(subtopic.id)}
                                    subtopicId={subtopic.id}
                                    after={refetchItems}/> 
                                : 
                                null
                            }
                            {
                                editItem && subtopic
                                ? 
                                <EditItemFormContainer
                                    close={()=>navSubtopic(subtopic.id)}
                                    subtopicId={subtopic.id}
                                    itemId={itemId}
                                    after={refetchItems}/> 
                                : 
                                null
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>  
            </span>  
        );
    }
    else {
        return (
            <i> selecione um subtópico </i>
        );
    }
}

export default SubtopicItemsList