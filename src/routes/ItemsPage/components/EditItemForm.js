import React, {Component} from 'react';
import { Segment, Header, Form, Label} from 'semantic-ui-react'

class EditItemForm extends Component {

	submit = (e) => {
		e.preventDefault()
		this.props.runMutation(
			()=>this.props.submit(this.props.data),
			()=>{this.props.afterEdit()}
		)
	}

    render() {
    	const {
    		close,
    		mutationLoaderAndError,
    		changeData,
    		data,
    		dataHasChanged
    	} = this.props;
		return (
			<div style={{position: 'relative'}}>
				{mutationLoaderAndError}
				<Header attached='top'>
					Editar Item
					<Label
						style={{marginTop: 1}}
						size='mini'
						attached='top right'
						as='a'
						onClick={close}
						icon={{ size: 'large', name: 'remove', style: {margin: 0} }}/>
				</Header>
				<Segment attached>
					<Form onSubmit={this.submit}>
							<Form.Input autoFocus
								name='newItem'
								onChange={(e,p)=>{changeData({name:p.value})}}
								placeholder={"nome"}
								value={data.name}
								action={{ icon: 'write', disabled: !dataHasChanged }}/>
					</Form>
				</Segment>
			</div>
		)
	}
}
export default EditItemForm





		