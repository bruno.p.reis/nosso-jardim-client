// React
import SUBTOPIC_QUERY from '../../../queries/Subtopic.graphql';
import { compose, graphql } from 'react-apollo';
import {navCreateItem,navEditItem,navSubtopic} from '../ducks.js'
import { connect } from 'react-redux';
import SubtopicPanel from './SubtopicPanel'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'

const withData = compose(
    graphql(
        SUBTOPIC_QUERY,
        {
            props:({data:{subtopic,loading,error}})=>{
                return {
                    subtopic,
                    loading,
                    error
                }
            },
            skip:(ownProps) => !ownProps.subtopicId,
            options:({subtopicId})=>({
                //forceFetch:true,
                variables:{
                    id: subtopicId
                }
            })
            
        }
    )
);

export default compose(
    withData,
    displayLoaderAndError,
    connect(
        null,
        (dispatch,ownProps) => ({
            navCreateItem:()=>navCreateItem(ownProps.subtopicId),
            navEditItem:(subtopicId,itemId)=>navEditItem(subtopicId,itemId),
            navSubtopic:(subtopicId=>navSubtopic(subtopicId)),
        })
    ),
)(SubtopicPanel)