import React from 'react';
import {List} from 'semantic-ui-react'
import ItemListItemContainer from "./ItemListItemContainer"
import { branch, renderComponent } from 'recompose';

const ItemsList = ({ 
    items,
    navEditItem,
    refetch,
    subtopicId
}) => {
    return (
        <List divided relaxed verticalAlign='middle'>
            {items.map(
                (item, i) =>
                    <ItemListItemContainer 
                        item={item} 
                        key={i} 
                        edit={()=>{navEditItem(subtopicId,item.id)}}
                        afterDelete={refetch}/>
            )}
        </List>            
    );
}

export default branch(
    ({items})=>items.length === 0,
    renderComponent(()=><span> No Items Yet </span>)
)(ItemsList)
