import React, {Component} from 'react';
import { Segment, Form, Label} from 'semantic-ui-react'

class CreateItemForm extends Component {

	submit = (e) => {
		e.preventDefault()
		this.props.runMutation(
			()=>this.props.submit(this.props.data)
		).then(()=>{
			this.props.resetData()
		})
	}

    render() {
    	const {
    		close,
    		mutationLoaderAndError,
    		changeData,
    		data,
    		dataHasChanged
    	} = this.props;
		return (
			<span>
				{mutationLoaderAndError}
				<Segment attached="top">
					Novo Item
					<Label
						style={{marginTop: 1}}
						size='mini'
						attached='top right'
						as='a'
						onClick={close}
						icon={{ size: 'large', name: 'remove', style: {margin: 0} }}/>
				</Segment>
				<Segment attached>
					<Form onSubmit={this.submit}>
							<Form.Input autoFocus
								name='newItem'
								onChange={(e,p)=>{changeData({name:p.value})}}
								placeholder={"nome"}
								value={data.name}
								action={{ icon: 'plus', disabled: !dataHasChanged }}/>
					</Form>
				</Segment>
			</span>
		)
	}
}
export default CreateItemForm





		