import React from 'react';
import { List, Button } from 'semantic-ui-react'
import ConfirmButton from '../../../components/ConfirmButton'

const ItemListItem = ({ 
    item:{name,id},
    deleteItem,
    edit,
    loaderAndError,
    runMutation
}) => {
    return (
        <List.Item key={id}>
            {loaderAndError}
            <List.Content floated='right'>
                <Button.Group compact size='mini'>
                    <Button 
                        icon='edit'
                        onClick={() => {edit(id)}}/>
                    <ConfirmButton 
                        executeAction={()=>{runMutation(deleteItem)}}
                        icon='trash'/>
                </Button.Group>
            </List.Content>
            <List.Content>
                <List.Header>
                    {name}
                </List.Header>
            </List.Content>
        </List.Item>
    )
}

export default ItemListItem;