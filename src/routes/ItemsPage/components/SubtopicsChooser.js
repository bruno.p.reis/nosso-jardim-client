import React from 'react';
import { Menu } from 'semantic-ui-react'
import { pure, compose } from 'recompose';

const SubtopicsChooser = ({ 
    subtopics, 
    selectedSubtopicId, 
    navSubtopic
}) => {
    let subtopicItems = null;
    if(subtopics) {
        subtopicItems = subtopics.map( (subtopic) =>
            <Menu.Item
                key={subtopic.id}
                index={parseInt(subtopic.id,10)}
                name={subtopic.name}
                active={selectedSubtopicId === subtopic.id}
                content={subtopic.name}
                onClick={(e,i)=>navSubtopic(i.index)} />
        )
    }
    return (
        <span>
            <Menu fluid vertical tabular>
                {subtopics ? subtopicItems : null}
            </Menu>
        </span>
    )
}

export default compose(pure)(SubtopicsChooser)