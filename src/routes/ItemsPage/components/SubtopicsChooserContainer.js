import { connect } from 'react-redux';
import { graphql } from 'react-apollo';
import SUBTOPICS_WITH_ITEMS_QUERY from '../queries/SubtopicsWithItems.graphql';
import SubtopicsChooser from './SubtopicsChooser'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'
import {compose} from 'recompose'

import {
    navSubtopic
} from '../ducks'

const withData = compose(
    graphql(
        SUBTOPICS_WITH_ITEMS_QUERY,
        {
            props:({data:{subtopics,loading,error},data})=>{
                return {
                    subtopics: subtopics,
                    loading,
                    error
                }
            },
            options:{
                fetchPolicy:'cache-and-network',
                variables:{
                    canHaveItems: true,
                    subtopicId: null
                }
            }
        }
    )
);

export default compose(
    connect(
        null,
        (dispatch) => ({
            navSubtopic
        })
    ),
    withData,
    displayLoaderAndError,
)(SubtopicsChooser)
    
