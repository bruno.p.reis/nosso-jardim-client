import ITEMS_QUERY from '../queries/Items.graphql';
import { compose, graphql } from 'react-apollo';
import {navEditItem} from '../ducks.js'
import { connect } from 'react-redux';
import ItemsList from './ItemsList'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'

const withData = graphql(
    ITEMS_QUERY,
    {
        props:({data:{refetch,items,loading,error}})=>{
            return {
                items,
                loading,
                error,
                refetch
            }
        },
        options:({subtopicId})=>({
            fetchPolicy: 'cache-and-network',
            variables:{
                subtopicId: subtopicId
            }  
        })
    }
)

export default compose(
    withData,
    displayLoaderAndError,
    connect(
        null,
        (dispatch,ownProps) => ({
            navEditItem:(subtopicId,itemId)=>navEditItem(subtopicId,itemId),
        })
    ),
)(ItemsList)