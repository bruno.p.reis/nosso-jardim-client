import { compose, graphql } from 'react-apollo';
import DELETE_MUTATION from '../mutations/delete.graphql';
import mutationRunner from '../../../HOCS/mutationRunner'
import ItemListItem from '../components/ItemListItem'

const withData =
    graphql(DELETE_MUTATION,{
        props: ({mutate,ownProps}) => ({
            deleteItem: () => {
                return mutate({
                    variables: {id:ownProps.item.id}
                })
                .then(()=>{
                    if(ownProps.afterDelete) {ownProps.afterDelete()}
                })
            }
        })
    }) 

export default compose(
    withData,
    mutationRunner
)(ItemListItem);