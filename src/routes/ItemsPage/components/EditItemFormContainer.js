import { graphql } from 'react-apollo';
import EDIT_MUTATION from '../mutations/edit.graphql';
import ITEM_QUERY from '../queries/Item.graphql';
import {compose} from 'react-apollo';
import _ from 'lodash'
import mutationRunner from '../../../HOCS/mutationRunner'
import dataManager from '../../../HOCS/dataManager'
import EditItemForm from '../components/EditItemForm'
import { connect } from 'react-redux';
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'

import {
    navSubtopic
} from '../ducks';

const withData = compose(
	graphql(
        ITEM_QUERY,
        {
            props:({data:{item,loading,error}})=>{
                return {
                    item,
                    loading,
                    error
                }
            },
            options:({itemId})=>({
                variables:{
                    id: itemId
                }
            })
            
        }
    ),
    graphql(EDIT_MUTATION,{
	    props: ({mutate, ownProps}) => ({
	        submit: 
		        ({id,name}) => {
		            return  mutate({
		                variables: {
		                	id:ownProps.itemId,
		                	name
		                }
		            })
		        }
	    })
	})
)

export default compose(
    connect(
        null,
        (dispatch, ownProps) => ({
            afterEdit: () => {
                navSubtopic(ownProps.subtopicId)
            }
        })
    ),
    withData,
	mutationRunner,
    displayLoaderAndError,
	dataManager({
        dataHash:{name:''},
        initialDataProp:'item'
    })
)(EditItemForm);

