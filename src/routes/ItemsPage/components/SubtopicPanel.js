import React, {Component} from 'react';
import {Grid,Header, Segment, Button} from 'semantic-ui-react'
import CreateItemFormContainer from './CreateItemFormContainer'
import EditItemFormContainer from './EditItemFormContainer'
import ItemsListContainer from './ItemsListContainer'

class SubtopicPanel extends Component {

    render () {
        const { 
            subtopic,
            navCreateItem,
            navSubtopic,
            createItem,
            editItem,
            itemId
        } = this.props;
        if(subtopic) {
            return (
                <span>
                    <Header content={subtopic.name} as="h3" style={{paddingBottom:'20px'}}/>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width='9'>
                                <Segment>
                                    <Grid style={{paddingBottom:'20px'}}>
                                        <Grid.Column width='8'>
                                            <Header as='h2'>
                                                Items
                                            </Header>
                                        </Grid.Column>
                                        <Grid.Column width='8'>
                                            <Button
                                                size='mini'
                                                primary
                                                floated='right'
                                                content='Adicionar Item'
                                                icon='add circle'
                                                labelPosition='right'
                                                onClick={navCreateItem}/>
                                        </Grid.Column>
                                    </Grid>
                                    <ItemsListContainer ref='list' subtopicId={subtopic.id}/>
                                </Segment>
                            </Grid.Column>
                            <Grid.Column width='5' verticalAlign='top'>
                                {
                                    createItem && subtopic
                                    ? 
                                    <CreateItemFormContainer
                                        close={()=>navSubtopic(subtopic.id)}
                                        subtopicId={subtopic.id}/> 
                                    : 
                                    null
                                }
                                {
                                    editItem && subtopic
                                    ? 
                                    <EditItemFormContainer
                                        close={()=>navSubtopic(subtopic.id)}
                                        subtopicId={subtopic.id}
                                        itemId={itemId}/> 
                                    : 
                                    null
                                }
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>  
                </span>  
            );
        }
        else {
            return (
                <i> selecione um subtópico </i>
            );
        }
    }
}

export default SubtopicPanel