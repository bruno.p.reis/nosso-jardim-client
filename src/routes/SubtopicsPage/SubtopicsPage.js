import React, { Component } from 'react'
import {  Button, Grid } from 'semantic-ui-react'

import SubtopicsTreeContainer from './components/SubtopicsTreeContainer'
import EditSubtopicFormContainer from './components/EditSubtopicFormContainer'
import CreateSubtopicFormContainer from './components/CreateSubtopicFormContainer'

import {
    navFirstLevelCreatePanel,
    navTree
} from './ducks';

class SubtopicsPage extends Component {
    render() {
        const {
            routeParams:{subtopicId},
            route:{
                create:createRoute,
                edit:editRoute
            }
        } = this.props;
        return (
            <span>
                <Grid padded>
                    <Grid.Row>
                        <Grid.Column width={(createRoute || editRoute) ? '10' : '16'}>
                            <div style={{marginLeft:'20px'}}>
                                <Button
                                    size='mini'
                                    primary
                                    content='Novo subtópico'
                                    icon='add circle'
                                    labelPosition='right'
                                    onClick={navFirstLevelCreatePanel}/>
                            </div>
                            <div style={{ height: 600, marginTop: 10 }}>
                                <SubtopicsTreeContainer subtopicId={subtopicId}/>
                            </div>
                        </Grid.Column>
                        { (createRoute || editRoute) &&
                            <Grid.Column width='6'>
                                { editRoute ?
                                    <EditSubtopicFormContainer 
                                        subtopicId={subtopicId}
                                        close={navTree}/> :
                                    null
                                }
                                { createRoute ?
                                    <CreateSubtopicFormContainer
                                        close={navTree}
                                        parentSubtopicId={subtopicId}/> :
                                    null
                                }
                            </Grid.Column>
                        }
                    </Grid.Row>
                </Grid>
            </span>

        )
    }
}
export default SubtopicsPage