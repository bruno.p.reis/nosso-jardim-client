import { browserHistory } from 'react-router'
import { createSelector } from 'reselect'
import _ from 'lodash'
const TOGGLE_NODE = 'nossoJardim/subtopics/TOGGLE_NODE';
const EXPAND_ANCESTORS = 'nossoJardim/subtopics/EXPAND_ANCESTORS';

// Actions
export function navFirstLevelCreatePanel() {
  const path = `/subtopics/create`
  browserHistory.push(path)
}

export function navCreatePanel(subtopicId) {
  const path = `/subtopics/${subtopicId}/create`
  browserHistory.push(path)
}

export function navEditPanel(subtopicId) {
  const path = `/subtopics/${subtopicId}`
  browserHistory.push(path)
}

export function navTree() {
  const path = `/subtopics`
  browserHistory.push(path)
}

export function toggleNode(subtopicId) {
    return {
        type: TOGGLE_NODE,
        subtopicId: subtopicId
    };
}

export function expandAncestors(subtopicId, includeItself = false) {
    return {
        type: EXPAND_ANCESTORS,
        subtopicId: subtopicId,
        includeItself: includeItself
    };
}

// ### ### ### Selectors

const apolloStateData = state => state.apolloState.data
const allStoreSubtopicsSelector = createSelector(
    apolloStateData,
    data => {
        const all = {}
        _.each(
            data,
            (obj,key)=>{
                if (key.indexOf('Subtopic_') === 0) {
                    all[obj.id] = obj
                }
            }
        )
        return all
    }
)

// ### ### ### Reducer

function subtopicsReducer(state = {expandedNodes:{}}, action, apolloState) {

    var parentSubtopic = (allSubtopics,subtopicId) => {
        return _.find(
            allSubtopics,
            (subtopic,x)=>{
                let has = false;
                _.each(
                    subtopic.subtopics,
                    (sub)=>{
                        if( sub.id === 'Subtopic_' + subtopicId ) {
                            has = true
                        }
                    }
                )
                return has;
            }
        ) 
    }

    let expanded, oldValue, parent;
    switch (action.type) {
        case TOGGLE_NODE:
            expanded = {}
            oldValue = state.expandedNodes[action.subtopicId];
            expanded[action.subtopicId] = oldValue ? false : true;
            state = Object.assign(
                {},
                state,
                {
                    expandedNodes: Object.assign({},state.expandedNodes,expanded)
                }
            )
            return state;
        case EXPAND_ANCESTORS:
            expanded = {}
            if(action.includeItself) {
                expanded[action.subtopicId] = true;
            }
            const all = allStoreSubtopicsSelector({apolloState})
            oldValue = null;
            parent = parentSubtopic(all,action.subtopicId);
            while(parent) {
                expanded[parent.id] = true;
                parent = parentSubtopic(
                    all,
                    parent.id
                )
            }
            state = Object.assign(
                {},
                state,
                {
                    expandedNodes: Object.assign({},state.expandedNodes,expanded)
                }
            )
            return state;
        default:
          return state;
    }
}
export default subtopicsReducer;