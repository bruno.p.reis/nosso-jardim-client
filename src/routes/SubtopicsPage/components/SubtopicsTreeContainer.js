//import update from 'immutability-helper'
import { connect } from 'react-redux';
import { compose, graphql } from 'react-apollo'

import SUBTOPICS_QUERY from '../../../queries/Subtopics.graphql';
import SUBTOPICS_MOVE_MUTATION from  '../mutations/SubtopicsMove.graphql';
import SUBTOPIC_DELETE_MUTATION from '../mutations/SubtopicDelete.graphql';
import SubtopicsTree from './SubtopicsTree'

import mutationRunner from '../../../HOCS/mutationRunner'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'

import {
    toggleNode,
    expandAncestors,
    navTree
} from '../ducks';

const withData = compose(
    graphql(
        SUBTOPICS_QUERY,
        {
            props:({
                    data:{subtopics,loading,error},
                    ownProps:{subtopicId}
            })=>{
                return {
                    subtopics,
                    loading,
                    error
                };
            },
            options: {
                fetchPolicy: 'cache-and-network',
            }
        }
    ),
    graphql(SUBTOPICS_MOVE_MUTATION,{
        props: ({mutate}) => ({
            move: (id,parentId,offset) => {
                return mutate({
                    variables: {id,parentId,offset}
                })
            }
        })
    }),
    graphql(SUBTOPIC_DELETE_MUTATION,{
        props: ({mutate,ownProps}) => ({
            subtopicDelete: (id) => {
                return mutate({
                    variables: {id}
                }).then(()=>{
                    let actualOpenedFormSubtopicId = ownProps.subtopicId
                    let removedId = id
                    if(actualOpenedFormSubtopicId === removedId) {
                        navTree()
                    }
                })
            }
        })
    })
)

export default compose (
    connect(
        (state,ownProps) => { 
            return {
                expandedNodes: state.subtopics.expandedNodes
            }
        },
        (dispatch) => ({
            toggleNode(subtopicId) {
                dispatch(toggleNode(subtopicId));
            },
            expandAncestors(subtopicId,includeItself) {
                dispatch(expandAncestors(subtopicId,includeItself));
            }
        })
    ),
    withData,
    mutationRunner,
    displayLoaderAndError
)(SubtopicsTree)
