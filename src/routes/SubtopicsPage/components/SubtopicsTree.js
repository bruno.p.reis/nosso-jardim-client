import React, { Component } from 'react'
import SortableTree, { walk } from 'react-sortable-tree'
import SubtopicActions from './SubtopicActions'

import {
    navCreatePanel,
    navEditPanel,
} from '../ducks';

class SubtopicsTree extends Component {

    componentWillReceiveProps(nextProps) {
        if(
            nextProps.subtopics !== this.props.subtopics &&
            nextProps.subtopicId
        ) {
            this.props.expandAncestors(nextProps.subtopicId)
        }   
    }

    onMoveNode = (data) => {
        let movedKey = data.node.key
        walk({
            treeData: data.treeData,
            getNodeKey: ({ node }) => node.key,
            ignoreCollapsed: true,
            callback: ({ node, parentNode }) => {
                if (node.key === movedKey) {
                    let parentKey = parentNode ? parentNode.key : '0'
                    let children = parentNode ? parentNode.children : data.treeData
                    let order = children.map( (child) => child.key )
                    this.move(movedKey, parentKey, order.indexOf(movedKey))
                    return false
                }
            },
        })
    }

    mapToTree = (subtopics) => {
        let treeData
        if ( subtopics.length ) {
            treeData = subtopics.map( (item) => {
                const tooLong = (item.name.length > 35)
                let tm = {
                    key: item.id,
                    name: item.name,
                    title: tooLong ? item.name.substring(0, 34) + '…' : item.name,
                    expanded: this.props.expandedNodes[item.id] || false,
                    canHaveItems: item.canHaveItems,
                    children: this.mapToTree(item.subtopics)
                }
                return tm
            })
        }
        return treeData
    }

    generateNodeProps = (rowInfo) => {
        let style = {}
        let actualSubtopic = rowInfo.node.key === this.props.subtopicId
        if (actualSubtopic) {
            style = {boxShadow: 'rgb(136, 136, 136) 5px 5px 5px'}
        }
        if(rowInfo.node.canHaveItems) {
            style['color'] = '#060';
        }
        return {
            style: style,
            buttons: [
                <SubtopicActions
                    navCreate={navCreatePanel}
                    navEdit={navEditPanel}
                    subtopicDelete={this.subtopicDelete}
                    key={rowInfo.node.key}
                    rowInfo={rowInfo}/>
            ],
        }
    }

    subtopicDelete = (id) => {
        this.props.runMutation(
            ()=>this.props.subtopicDelete(id)
        )
    }

    move = (id, parentId, newOffset) => {
        this.props.runMutation(
            ()=>this.props.move(id, parentId, newOffset)
        )
    }

    render() {
        const {
            subtopics,
            mutationLoaderAndError
        } = this.props

        return (
            <span>
                {mutationLoaderAndError}
                { 
                    !!subtopics.length
                    ?
                    <SortableTree
                        treeData={this.mapToTree(subtopics[0]['subtopics'])}
                        getNodeKey={({ node }) => node.key}
                        onChange={()=>{}}
                        onMoveNode={this.onMoveNode}
                        onVisibilityToggle={(data) => this.props.toggleNode(data.node.key)}
                        maxDepth={3}
                        generateNodeProps={this.generateNodeProps}/>
                    :
                    <i>Ainda não há subtópicos cadastrados.</i>     
                }
            </span>
        )
    }
}

export default SubtopicsTree