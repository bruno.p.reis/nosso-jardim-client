import React, { Component } from 'react'
import { Segment, Form, Label, Header } from 'semantic-ui-react'
import BreadCrumbs from './BreadCrumbs'

class CreateSubtopicForm extends Component {

	componentDidUpdate = () => {
		document.querySelector("form input").focus()
	}

    submit = (e) => {
    	e.preventDefault();
    	const {
    		data:{name},
    		parentSubtopicId
    	} = this.props;
    	let action =  
    		this.props.parent 
    		?
    		()=>this.props.subtopicRegister(parentSubtopicId,name)
    		:
        	()=>this.props.subtopicRegisterFirstLevel(name)

    	this.props.runMutation(action)
    	.then(
    		()=>{
    			this.props.resetData()
    			this.props.expandAncestors(parentSubtopicId,true)
    		}
    	)
    }

    render() {
    	const {
    		parent,
    		close,
    		mutationLoaderAndError,
    		changeData,
    		data,
    		dataHasChanged
    	} = this.props
    	return (
			<div style={{position: 'relative'}}>
				{mutationLoaderAndError}
				<Header block as='h4' attached='top'>
					<BreadCrumbs subtopic={parent} includeActual={true}/>
					Novo subtópico:
					<Label
						style={{marginTop: 1}}
						size='mini'
						attached='top right'
						as='a'
						onClick={close}
						icon={{ size: 'large', name: 'remove', style: {margin: 0} }}/>
				</Header>
				<Segment attached>
					<Form onSubmit={this.submit}>
						<Form.Input autoFocus
							name='newSubtopic'
							onChange={
								(e,p)=>changeData({name:p.value})
							}
							placeholder='Nome do subtópico'
							value={data.name}
							action={{ icon: 'plus', disabled: !dataHasChanged }}/>
		    		</Form>
				</Segment>
			</div>
    	)
    }
}

export default CreateSubtopicForm

