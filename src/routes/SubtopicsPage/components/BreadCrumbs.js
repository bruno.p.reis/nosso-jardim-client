import React from 'react'
import { Header } from 'semantic-ui-react'

const BreadCrumbs = ({subtopic,includeActual}) => {
	if (subtopic && subtopic.ancestors) {
		let ancestors = subtopic.ancestors.slice(1) // remove root
        if(includeActual) {
        	ancestors.push(subtopic)
        }
		return (
			<Header.Subheader size='small'>
				{
					ancestors.map( (ancestor, i) => {
						const divider = (i===0) ? '' : '>'
						return (
							<span key={i}>
								{divider} {ancestor.name}&nbsp;
							</span>
						)
					})
				}
			</Header.Subheader>
		)
	}
	return null;
}

export default BreadCrumbs

