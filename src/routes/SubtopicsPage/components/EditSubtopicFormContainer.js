import SUBTOPIC_EDIT_MUTATION from '../mutations/SubtopicEdit.graphql'
import SUBTOPIC_QUERY from '../../../queries/Subtopic.graphql';
import { compose, graphql } from 'react-apollo';
import EditSubtopicForm from './EditSubtopicForm'
import _ from 'lodash'
import mutationRunner from '../../../HOCS/mutationRunner'
import dataManager from '../../../HOCS/dataManager'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'

const withData = compose(
    graphql(
        SUBTOPIC_QUERY,
        {
            props:({data:{subtopic,loading,error}})=>{
                return {
                    subtopic,
                    loading,
                    error
                }
            },
            skip:(ownProps) => !ownProps.subtopicId,
            options:({subtopicId})=>({
                fetchPolicy: 'cache-and-network',
                variables:{
                    id: subtopicId
                }
            })
            
        }
    ),
  	graphql(SUBTOPIC_EDIT_MUTATION,{
        props: ({mutate}) => ({
            subtopicsEdit: (id,name,canHaveItems) => {
                return mutate({
                    variables: {id,name,canHaveItems}
                })
            }
        })
    })
)
export default compose(
    withData,
    mutationRunner,
    displayLoaderAndError,
    dataManager({
        dataHash:{name:"",canHaveItems:false},
        initialDataProp:'subtopic'
    })
)(EditSubtopicForm)

