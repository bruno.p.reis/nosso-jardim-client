import React, { Component } from 'react'
import BreadCrumbs from './BreadCrumbs'

import {
	Segment,
	Form,
	Label,
	Header
} from 'semantic-ui-react'

class EditSubtopicForm extends Component {

	componentDidUpdate = () => {
		document.querySelector("form input").focus()
	}

	submit = (e) => {
		e.preventDefault()
		const {
			data:{name,canHaveItems},
			subtopic,
			subtopicsEdit		
		} = this.props;
		this.props.runMutation(
			()=>subtopicsEdit(
                subtopic.id,
                name,
                canHaveItems
            )
		)
	}

    render() {
    	const {
    		subtopic,
    		data,
    		dataHasChanged,
    		close,
    		mutationLoaderAndError,
    		changeData
    	} = this.props;
    	return (
			<div style={{position: 'relative'}}>
				{mutationLoaderAndError}
				<Header block as='h4' attached='top'>
					<BreadCrumbs subtopic={subtopic} includeActual={false}/>
					{subtopic ? subtopic.name : ''}
					<Label
						style={{marginTop: 1}}
						size='mini'
						attached='top right'
						as='a'
						onClick={close}
						icon={{ size: 'large', name: 'remove', style: {margin: 0} }}/>
				</Header>
				<Segment attached>
					<Form onSubmit={this.submit}>
						<Form.Input autoFocus
							name='name'
							onChange={(e,p) => changeData({name:p.value})}
							value={data.name}
							action={{ icon: 'checkmark', disabled: !dataHasChanged }}/>
						<Form.Checkbox 
							name='canHaveItems'
							onChange={(e,p) => changeData({canHaveItems:p.checked})}
							label='This subtopic can have items.'
							checked={data.canHaveItems}/>
					</Form>
				</Segment>
			</div>
    	)
    }
}

export default EditSubtopicForm
