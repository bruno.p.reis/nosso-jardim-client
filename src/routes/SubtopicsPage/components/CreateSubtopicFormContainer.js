import SUBTOPIC_REGISTER_MUTATION from '../mutations/SubtopicRegister.graphql'
import SUBTOPIC_REGISTER_FIRST_LEVEL_MUTATION from '../mutations/SubtopicRegisterFirstLevel.graphql'
import SUBTOPIC_QUERY from '../../../queries/Subtopic.graphql';
import { compose, graphql } from 'react-apollo';
import CreateSubtopicForm from './CreateSubtopicForm'
import mutationRunner from '../../../HOCS/mutationRunner'
import dataManager from '../../../HOCS/dataManager'
import { connect } from 'react-redux';

import {
    expandAncestors
} from '../ducks';

const withData = compose(
    graphql(
        SUBTOPIC_QUERY,
        {
            props:({data:{subtopic,loading,error}})=>{
                return {
                    parent:subtopic,
                    loading,
                    error
                }
            },
            skip:(ownProps) => !ownProps.parentSubtopicId,
            options:(ownProps)=>{
                return {
                    variables:{
                        id: ownProps.parentSubtopicId
                    }
                }
            }
            
        }
    ),
  	graphql(SUBTOPIC_REGISTER_MUTATION,{
	    props: ({mutate}) => ({
	        subtopicRegister: (parentId,name) => {
	            return mutate({
	                variables: {parentId,name}
	            })
	        }
	    })
	}),
  	graphql(SUBTOPIC_REGISTER_FIRST_LEVEL_MUTATION,{
	    props: ({mutate}) => ({
	        subtopicRegisterFirstLevel: (name) => {
	            return mutate({
	                variables: {name}
	            }); 
	        }
	    })
	})
)

export default compose(
    withData,
    mutationRunner,
    dataManager({
        dataHash:{name:''}
    }),
    connect(
        null,
        (dispatch) => ({
            expandAncestors(subtopicId,includeItself) {
                dispatch(expandAncestors(subtopicId,includeItself));
            }
        })
    ),
)(CreateSubtopicForm)


