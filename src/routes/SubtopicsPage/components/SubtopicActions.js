import React, {PropTypes} from 'react'
import { Button } from 'semantic-ui-react'
import ConfirmButton from '../../../components/ConfirmButton'

const SubtopicActions = (
    {
        rowInfo:{
            node:{key:subtopicId}
        },
        navCreate,
        navEdit,
        subtopicDelete
    }
) => {
	return (
        <div>
            <Button.Group size='mini'>
    			<Button compact
                    icon='plus'
                    onClick={()=>navCreate(subtopicId)}/>
                <Button compact
                    icon='write'
                    onClick={()=>navEdit(subtopicId)}/>
                <ConfirmButton 
                    executeAction={()=>{subtopicDelete(subtopicId)}}
                    icon='trash outline'/>
            </Button.Group>
        </div>
	)
}

SubtopicActions.propTypes = {
    subtopicDelete: PropTypes.func.isRequired,
    navEdit: PropTypes.func.isRequired,
    navCreate: PropTypes.func.isRequired,
    rowInfo: PropTypes.any.isRequired
}
export default SubtopicActions