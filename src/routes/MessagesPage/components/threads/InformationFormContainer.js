import INFORMATION_REGISTER_FOR_THREAD_MUTATION from '../../mutations/informationRegisterForThread.graphql'
import SUBTOPICS_QUERY from '../../../../queries/Subtopics.graphql';
import { compose, graphql } from 'react-apollo';
import InformationForm from './InformationForm'
import mutationRunner from '../../../../HOCS/mutationRunner'
import dataManager from '../../../../HOCS/dataManager'
import displayLoaderAndError from '../../../../HOCS/displayLoaderAndError'

const withData = compose(
    graphql(
        SUBTOPICS_QUERY,
        {
            props:({data:{subtopics,loading,error}})=>{
                return {
                    subtopics,
                    loading,
                    error
                }
            }
        }
    ),
    graphql(INFORMATION_REGISTER_FOR_THREAD_MUTATION, {
        props: ({ mutate }) => ({
            informationRegisterForThread: ( threadId, information ) => mutate({
                variables: { threadId:threadId, information:information }
            })
        })
    }),
)
export default compose(
    withData,
    mutationRunner,
    displayLoaderAndError,
    dataManager({
        dataHash:{subtopicId:""}
    })
)(InformationForm)