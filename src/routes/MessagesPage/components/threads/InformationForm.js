import React from 'react'
import { Button, Form, Dropdown } from 'semantic-ui-react'
import SimpleForm from '../../../../components/SimpleForm'
import _ from 'lodash'
export default (props) => {

    const {
        setFormOpened,
        data:{subtopicId},
        disabledSubtopics,
        changeData,
        subtopics,
        threadId,
        runMutation,
        informationRegisterForThread,
        mutationLoaderAndError
    } = props
    
    
    
    
    const options = () => {
        let options = []
        const rootSubtopics = subtopics[0].subtopics;
        const traverse = (subtopics) => {
            subtopics.forEach((s)=> {
                const ancestorsButRootNames = _.map(
                    s.ancestors,
                    (a)=>a.name
                ).slice(1)

                let name = _.join([...ancestorsButRootNames,s.name],' > ')
                if(s.canHaveItems) {
                    name += ' *'
                }
                options.push({
                    text:name,
                    value:s.id,
                    disabled: _.includes(disabledSubtopics,s.id)
                })
                if(s.subtopics && s.subtopics.length > 0) {
                    traverse(s.subtopics)
                }
                
            })
        }
        traverse(rootSubtopics)
        return options;
    }
    // if(rootSubtopics.length && rootSubtopics.length>0) {
    //     rootSubtopics.forEach((s)=> {
    //         options.push({text:s.name,value:s.id})
    //         s.subtopics.forEach((s)=>
    //             options.push({text:s.name,value:s.id})
    //         )
    //     });
    // }

    const submit = (e) => {
        e.preventDefault();
        runMutation(
            ()=>
                informationRegisterForThread(
                    threadId,{subtopicId:subtopicId}
                ),
            ()=>changeData({subtopicId:""})
        )
    }

    return (
        <SimpleForm
            style={{margin: "10px"}}
            title="Identificar Informação"
            onClick={(e)=>{e.stopPropagation()}}
            onSubmit={submit}
            close={(e)=>setFormOpened(false)}>
            {mutationLoaderAndError}
            <Dropdown
                autoFocus
                fluid 
                search 
                selection 
                onChange={
                    (a,b)=>{
                        changeData({subtopicId:b.value})
                    }
                } 
                name="subtopicId"
                style={{marginBottom:"10px"}}
                value={subtopicId}
                size="small" 
                options={options()} 
                placeholder='Em que subtópico você categoriza esta informação?' />

            <Form.TextArea 
                name='about' 
                placeholder='Caso caiba, explique melhor como estas informações se relacionam ao tópico...' />

            {/**<Dropdown
                fluid 
                search 
                selection 
                onChange={
                    (a,b)=>{
                        changeData({})
                    }
                } 
                name="itemId"
                style={{marginBottom:"10px"}}
                value={''}
                size="small" 
                options={options()} 
                placeholder='Item' />**/}
            
                <Button type='submit' disabled={subtopicId === ''} primary size='small'>ok</Button>
                <Button size='small' onClick={()=>setFormOpened(false)}>cancel</Button>
            
        </SimpleForm>
    )

}   