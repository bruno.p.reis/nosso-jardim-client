import React from 'react'
import Message from '../Message.js'
import { Comment, Icon } from 'semantic-ui-react'

const ThreadMessages = ({
	collapsed, 
	messages,
	cleanMessages,
}) => {
	
	const ellipsisStyle = {
		display: 'block',
		textAlign: 'center',
		lineHeight: '0',
		color: '#999'
	}
	
	let firstAndLast = (messages) => messages.filter(
		(_, i) => (i === 0 || i === messages.length - 1)
	)

	let injectElipsis = (messagesComponents,numMessages) => 
		[
			messagesComponents[0],
			<span key='elipsis' style={ellipsisStyle}>
				<Icon name='ellipsis vertical'/>
			</span>,
			messagesComponents[1]
		]

	let messagesToShow

	if(collapsed) {
		messagesToShow = firstAndLast(messages)
	}
	else {
		messagesToShow = messages
	}

	let messagesComponents = messagesToShow.map( 
		(message, i) => 
			<Message
				cleanMessages={cleanMessages}
				message={message}
				insideThreadMessages={true}
				collapsedThread={collapsed}
				showRemoveButton={!collapsed}
				key={message.id} />
	)

	if ( collapsed && messages.length > 2 ) {
		messagesComponents = injectElipsis(messagesComponents,messages.length)
	}

    return <Comment.Group className='selection' children={messagesComponents} />
}

export default ThreadMessages
