import React, { Component } from 'react'
import { Icon, Label, Button } from 'semantic-ui-react'
import InformationLabelContainer from './InformationLabelContainer'
import Moment from 'moment'
import 'moment/locale/pt-br'
import {withState} from 'recompose'
import InformationFormContainer from './InformationFormContainer'
import _ from 'lodash'
class ThreadHeader extends Component {

    constructor(props) {
        super(props)
        Moment.locale('pt-br')
    }

    renderMediaTypes = () => {
        const uniqueTypes = [...new Set(this.props.mediaTypes)]
        const mediaIcons = uniqueTypes.map( (mediaType, i) => {
            let iconName
            if (mediaType === 'Photo') iconName = 'file image outline'
            else if (mediaType === 'Document') iconName = 'file text outline'
            else if (mediaType === 'Audio') iconName = 'file audio outline'
            else if (mediaType === 'Video') iconName = 'file video outline'
            return <Icon name={iconName} key={i}/>
        })
        return <div style={{float: 'right'}}>{mediaIcons}</div>
    }

    render() {

        const {
            thread:{id,messages,informations},
            collapsed,
            selectedMessagesCount,
            addMessagesToThread,
            formOpened,
            setFormOpened
        } = this.props

        let addMessagesToThreadButton = () => {
            if(collapsed || selectedMessagesCount ===0) {
                return null;
            }
            else {
                let text = "Adicinar " +
                (   
                    selectedMessagesCount > 1 ?
                        selectedMessagesCount + " Mensagens Selecionadas":
                        'Mensagem Selecionada'
                );
                return (
                    <div style={{clear:"both",textAlign:"center"}}>
                        <br/>   
                        <Button primary size='mini' onClick={addMessagesToThread}>
                            {text}
                        </Button>
                    </div>
                )
            }
        }

        let addInformationButton = () => {
            return collapsed || formOpened ? null : (
                <Label 
                    color="blue" 
                    as="a" 
                    onClick={(e)=>{
                        e.stopPropagation()
                        setFormOpened(true)
                    }}
                    >
                    <Icon name="tag"/> + 
                </Label>
            );
        }

        const infos = () => {
            return (
                <Label.Group>
                    <Label color="teal">
                        {messages.length}
                    </Label>
                    {_.map(
                        informations,
                        (i,o)=> <InformationLabelContainer {...{collapsed}} key={o} information={i}/>
                    )}
                    {addInformationButton()}
                </Label.Group>
            );
        }

           

        return (
            <div>
                <span>
                    {infos()}
                </span>
                {addMessagesToThreadButton()}
                {
                    (!formOpened || collapsed) ? 
                    null : 
                    <InformationFormContainer 
                        threadId={id} 
                        disabledSubtopics={_.map(informations,(i)=>i.subtopic.id)}
                        {...{setFormOpened}}/>
                }
            </div>
        )

    }
}
const enhance = withState('formOpened', 'setFormOpened', false);

export default enhance(ThreadHeader)