import INFORMATION_REMOVE from '../../mutations/informationRemove.graphql'
import { compose, graphql } from 'react-apollo';
import InformationLabel from './InformationLabel'
import mutationRunner from '../../../../HOCS/mutationRunner'

const withGQL = compose(
    graphql(INFORMATION_REMOVE, {
        props: ({ mutate }) => ({
            informationRemove: ( informationId ) => mutate({
                variables: { id:informationId }
            })
        })
    }),
)

export default compose(
    mutationRunner,
    withGQL
)(InformationLabel)