import React from 'react'
import { Icon, Label } from 'semantic-ui-react'

export default ({
	collapsed,
	information,
	informationRemove,
	runMutation,
	mutationLoaderAndError
}) => {
	
	const removeInformation = (e) => {
		e.stopPropagation()
		runMutation(()=>informationRemove(information.id))
	}

	const extra = collapsed ? 
		null : 
		<a>
			<Icon 
				name='remove' 
				onClick={removeInformation}
				style={{marginRight:"-2px",marginLeft:"5px"}}/>
		</a>


	return (
		<Label>
			{mutationLoaderAndError}
			{information.subtopic.path}
			{extra}
		</Label>
	);
}