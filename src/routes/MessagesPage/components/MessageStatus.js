import React from 'react';
import ActionsContainer from './ActionsContainer'
import { Header, Form, Progress } from 'semantic-ui-react'
import Moment from 'moment'

const MessageStatus = (
	{
		selectedMessages,
		peers,
		peerId,
		setPeer,
		messagesStatus:ms,
		clearSelection
	},
	{t}
) => {
	
	let text = t("Mensagens")
	if(selectedMessages.length === 1) {
		text += " - 1"
	}
	if(selectedMessages.length > 1) {
		text += ' - ' + selectedMessages.length
	}

	let from,until,progress,options;
	
	options = []
	if(peers) {
		options = peers.map(
			(p)=>({text:p.printName,value:p.id})
		)
	}

	if(ms) {
		from = Moment(ms.oldestMessageDate).format(t('D/M/Y'))
		until = Moment(ms.newestMessageDate).format(t('D/M/Y'))
		let done = ms.messagesCount - ms.cleanMessagesCount
		progress = (done/ms.messagesCount ) * 100
	}

	let messagesCount = (ms) =>
		<span>
			{ms.cleanMessagesCount}
			&nbsp;de&nbsp;
			{ms.messagesCount}
		</span>

	let messagesDates = (ms) =>
		<span>
		{t('de')} {from} {t('a')} {until} 
		</span>

    return (
    	<div>
	    	<Header as='h3' floated="left" style={{marginBottom:"2px",width:"27%",display:"inline-block"}}>
	    		{text}
	    		<Header.Subheader>

    			 	<Progress color="teal" size="tiny" percent={progress} style={{margin:"0px 20px 0px 0px",display:"inline-block", width:"150px"}}/>
	    		</Header.Subheader>
	    	</Header>
	    	<div style={{textAlign:"center",marginBottom:"2px",width:"32%",display:"inline-block"}}>
    			{ ms ? 
    				<div style={{fontSize:'0.9em', marginTop:"4px"}}>
    					{messagesCount(ms)}<br/>
    					&nbsp;&nbsp;&nbsp; 
    					<span style={{color:"#bbb"}}> {messagesDates(ms)} </span>
    				</div>
    				:
    				null
    			}
	    	</div>
	    	<div style={{float:"right", marginBottom:"15px",width:"35%",textAlign:"right"}}>
	    		<Form.Select 
	    			onChange={
	    				(a,b)=>{
	    					setPeer(b.value)
	    				}
	    			} 
	    			value={peerId}
	    			size="small" options={options} 
	    			placeholder='Canal' />
	    	</div>
	    	<ActionsContainer {...{clearSelection}} disabled={selectedMessages.length===0}/>
	    </div>
	)
}

MessageStatus.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default MessageStatus