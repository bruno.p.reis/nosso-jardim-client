import React from 'react'
import { Header, Icon } from 'semantic-ui-react'

export default () => 
    <Header as='h2' icon textAlign='center' style={{marginTop:"30px"}}>
		<Icon name='info' circular />
		<Header.Content>
			Não há mensagens para organizar. <br/>
			Você precisa primeiro importá-las do Telegram. 
		</Header.Content>
    </Header>

