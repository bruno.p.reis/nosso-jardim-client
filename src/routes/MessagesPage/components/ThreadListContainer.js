import React from 'react'
import Help from '../components/Help'
import { compose, graphql } from 'react-apollo'
import { connect } from 'react-redux';
import THREADS_QUERY from '../queries/threads.graphql'
import ThreadList from '../components/ThreadList'
import MESSAGES_CLEAN_MUTATION from '../mutations/messagesClean.graphql'
import THREAD_ADD_MESSAGES_MUTATION from '../mutations/threadAddMessages.graphql'
import CLEAN_MESSAGES_STATUS_QUERY from '../queries/cleanMessagesStatus.graphql'
import THREADS_STATUS_QUERY from '../queries/threadsStatus.graphql'
import {clearSelection} from '../ducks'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'
import {branch,renderComponent} from 'recompose'

export default compose(
    connect(
        (state,ownProps) => ({ 
            peerId: state.messages.peerId,
            selectedMessagesCount: state.messages.selectedMessages.length,
            selectedMessages: state.messages.selectedMessages
        }),
        (dispatch) => ({
            clearSelection: () => dispatch(clearSelection())
        })
    ),
    graphql(THREAD_ADD_MESSAGES_MUTATION, {
        props: ({ mutate, ownProps }) => ({
            addMessagesToThread: ( threadId ) => 
                mutate({
                    refetchQueries: [
                        {
                            query: CLEAN_MESSAGES_STATUS_QUERY,
                            variables: { peerId: ownProps.peerId },
                        },
                        {
                            query: THREADS_STATUS_QUERY,
                            variables: { peerId: ownProps.peerId },
                        }
                    ],
                    variables: { 
                        messageIds:ownProps.selectedMessages, 
                        threadId 
                    }
                }).then(
                    ()=>{
                        ownProps.clearSelection()
                    }
                )
        })
    }),
    graphql(
        THREADS_QUERY,
        {
            options: ({peerId}) => ({variables:{peerId:peerId}}),
            skip: (ownProps) => ownProps.peerId === '',
            props: ({ownProps,data:{loading,error,threads}}) => {
                let ret = {
                    loading,
                    error,
                    threads,
                };
                return ret;
            }
        }
    ),
    graphql(MESSAGES_CLEAN_MUTATION, {
        props: ({ mutate, ownProps }) => ({
            cleanMessages: ( messageIds ) => {
                return mutate({
                    variables: { messageIds:[messageIds] },
                    refetchQueries: [
                        {
                            query: CLEAN_MESSAGES_STATUS_QUERY,
                            variables: { peerId: ownProps.peerId },
                        },
                        {
                            query: THREADS_QUERY,
                            variables: { peerId: ownProps.peerId },
                        },
                        {
                            query: THREADS_STATUS_QUERY,
                            variables: { peerId: ownProps.peerId },
                        }
                    ],
                })
            }
        })
    }),
    displayLoaderAndError,
    branch(
        (data)=> data.threads === undefined,
        renderComponent(()=><Help
            instructions={[
                'Escolha um canal',
                'escolha um canal na caixa acima para ler as conversas'
        ]}/>)
    ),
    branch(
        ({threads})=>threads.length===0,
        renderComponent(()=><Help
            instructions={[
                'Para criar uma conversa',
                'selecione as mensagens correspondentes',
                'aperte em "Nova Conversa"'
        ]}/>)
    )
)(ThreadList)