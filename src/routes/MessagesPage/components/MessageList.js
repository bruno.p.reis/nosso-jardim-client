import React, { Component } from 'react'
import { Comment, Button, Container } from 'semantic-ui-react'
import Message from "../components/Message"
import {branch,renderComponent} from 'recompose'

const PreviousButton = ({hasPreviousPage,loadPreviousEntries,t}) => 
    <Container textAlign="center" style={{marginTop:"20px"}}>
        <Button size="mini" circular basic onClick={loadPreviousEntries} content={t("anteriores")} icon="chevron up"/>
    </Container>

const NextButton = ({hasNextPage,loadMoreEntries,t}) =>
    <Container textAlign="center"  style={{marginBottom:"20px"}}> 
        <Button size="mini" circular basic onClick={loadMoreEntries} content={t("próximas")} icon="chevron down"/>
    </Container>

const NoButton = ()=> null

const BranchPreviousButton = branch(
    ({hasPreviousPage})=>!hasPreviousPage,
    renderComponent(NoButton)
)(PreviousButton)

const BranchNextButton = branch(
    ({hasNextPage})=>!hasNextPage,
    renderComponent(NoButton)
)(NextButton)

class MessageList extends Component {
    render() {
        const {
            messages,
            selectedMessages,
            toggleMessage,
        } = this.props;
        const{t} = this.context;

        let isSelected = (message) => selectedMessages.indexOf(message.id) !== -1
        
        const messagesC = messages.map(
            (message) => {
                let belongsToThread = !!(message.thread && message.thread.id)
                return (
                    <Message
                        onClick={()=>toggleMessage(message.id)}
                        message={message}
                        selected={isSelected(message)}
                        belongsToThread={belongsToThread}
                        key={message.id}/>
                );
            }
        )
        return (
            <span>
                <BranchPreviousButton {...this.props} t={t}/>
                <Comment.Group
                    className='selection list'
                    children={messagesC}/>
                <BranchNextButton {...this.props} t={t}/>
            </span>
        )

    }
}

MessageList.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default MessageList;