import { compose, graphql } from 'react-apollo'
import { connect } from 'react-redux';
import update from 'immutability-helper';
import THREAD_REGISTER_MUTATION from '../mutations/threadRegister.graphql';
import MESSAGES_MARK_IRRELEVANT_MUTATION from '../mutations/messagesMarkIrrelevant.graphql';
import Actions from '../components/Actions'
import {clearSelection} from '../ducks'
import CLEAN_MESSAGES_STATUS_QUERY from '../queries/cleanMessagesStatus.graphql'
import THREADS_STATUS_QUERY from '../queries/threadsStatus.graphql'
import messagesQueryWrapper from '../queries/messagesQueryWrapper'

export default compose(
    connect(
        (state,ownProps) => ({ 
            selectedMessages: state.messages.selectedMessages,
	        peerId: state.messages.peerId
        }),
        (dispatch) => ({
            clearSelection: () => dispatch(clearSelection())
        })
    ),
	graphql(MESSAGES_MARK_IRRELEVANT_MUTATION, {
	    props: ({ mutate, ownProps }) => ({
	        markIrrelevantMessages: ( messageIds ) => mutate({
	            variables: { messageIds:ownProps.selectedMessages },
	            refetchQueries: [{
				    query: CLEAN_MESSAGES_STATUS_QUERY,
				    variables: { peerId: ownProps.peerId },
				}],
	        })
	        .then(ownProps.clearSelection)
	    })
	}),
	graphql(THREAD_REGISTER_MUTATION, {
	    props: ({ mutate,ownProps }) => ({
	        registerThread: ( messageIds ) => mutate({
	            variables: { messageIds:ownProps.selectedMessages },
	            refetchQueries: [
		            {
					    query: CLEAN_MESSAGES_STATUS_QUERY,
					    variables: { peerId: ownProps.peerId },
					},
                    {
                        query: THREADS_STATUS_QUERY,
                        variables: { peerId: ownProps.peerId },
                    }
				],
	            updateQueries: {
	                threads: (prev, { mutationResult }) => {
	                    let newThread = mutationResult.data.threadRegister
	                    return update(prev, {
	                        threads: { $unshift: [newThread] },
	                    })
	                },
	            }
	        })
	        .then(ownProps.clearSelection)
	    })
	}),
	messagesQueryWrapper,
)(Actions)

