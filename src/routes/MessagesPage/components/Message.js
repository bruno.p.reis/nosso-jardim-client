import React, {PropTypes} from 'react'
import { Comment } from 'semantic-ui-react'
import Moment from 'moment'
import 'moment/locale/pt-br'
import AvatarImage from './message/AvatarImage'
import Media from './message/Media'
import MessageButtonsContainer from './message/MessageButtonsContainer'
import anchorme from "anchorme";

const truncate = (str, size) => {
	return str.length > size ?
		str.substring( 0 , size ) + '...' :
		str
}
const renderMedia = ({message:{media,thread},collapsedThread}) =>
		<Media {...{media,collapsedThread}} threaded={!!thread}/>

const addExternalLink = (text) => {
	return {
		__html:anchorme(
			text,
			{
 			    attributes:[
			        {
			            name:"target",
			            value:"_blank",
			        }
			    ],
			}
		)
	}
}
const renderText = ({message,collapsedThread}) =>
	<Comment.Text style={{fontSize: '1.1em', whiteSpace: 'pre-line'}}>
		{collapsedThread ? 
			<span>{truncate(message.text,70)}</span> : 
			<span dangerouslySetInnerHTML={addExternalLink(message.text)}/>
		}
	</Comment.Text>

const Message = (
	{
		showRemoveButton, 
		message, 
		onClick, 
		selected,
		collapsedThread,
		insideThreadMessages
	},
	{t}
) => {

	function calculateCommentClass() {
		let commentClass = ['item']
		if (!insideThreadMessages && message.thread) commentClass.push('disabled', 'belongsToThread')
		if (message.irrelevant) commentClass.push('irrelevant','disabled')
		if (selected) commentClass.push('active')
		return commentClass.join(' ')
	}
	
	let timestamp = Moment(message.sentAt).format(t('D/M/Y')+' H:mm')

	function click(e) {
		if(e.target.tagName === 'A') {
			// do not propagate
		}
		else {
			if(onClick) onClick()
		}
	}
	
	return(
			<Comment style={{borderBottom:"solid #f4f4f4 1px"}} className={calculateCommentClass()} onClick={click}>
	      		<AvatarImage {...{collapsedThread:false,message}}/>
				<Comment.Content>
					<Comment.Author
						as='span'
						style={{fontSize: '.8em'}}
						children={message.sender.name}/>
					<Comment.Metadata as='span' children={timestamp}/>
					{ message.media ? renderMedia({message,collapsedThread}) : null}
					{ message.text ? renderText({message,collapsedThread}) : null}
					<MessageButtonsContainer {...{collapsedThread,showRemoveButton,message}}/>
				</Comment.Content>
			</Comment>
	);
}

Message.contextTypes = {
  t: React.PropTypes.func.isRequired
}

Message.propTypes = {
    showRemoveButton: PropTypes.bool,
    message: PropTypes.object.isRequired,
    onClick: PropTypes.func,
    selected: PropTypes.bool,
    collapsedThread: PropTypes.bool,
    insideThreadMessages: PropTypes.bool
};

export default Message