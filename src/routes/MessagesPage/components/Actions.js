import React from 'react';
import { Button, Icon } from 'semantic-ui-react'


const Actions = ({
		registerThread,
		markIrrelevantMessages,
		clearSelection,
		disabled,
		refetchMessages
	},
	{t}
) => {
    return (
		<Button.Group fluid>
	    	<Button primary onClick={registerThread} disabled={disabled}>
	      		{t('Criar Nova Conversa')}
	      	</Button>
			<Button onClick={markIrrelevantMessages} disabled={disabled}>
	      		{t('Marcar como Irrelevantes')}
	      	</Button>
			<Button onClick={clearSelection} disabled={disabled}>
	      		{t('Desselecionar')}
	      	</Button>
	      	<Button icon onClick={()=>{refetchMessages()}}>
	      		<Icon name='refresh' />
	      	</Button>
		</Button.Group>
    );
}


Actions.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default Actions