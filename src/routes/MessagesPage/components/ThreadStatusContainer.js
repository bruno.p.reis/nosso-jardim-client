import React from 'react';
import { compose, graphql } from 'react-apollo'
import { connect } from 'react-redux';
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'
import { Header } from 'semantic-ui-react'
import THREADS_STATUS_QUERY from '../queries/threadsStatus.graphql'
import Moment from 'moment'

const ThreadStatus = (
	{
		messagesStatus:ms
	},
	{t}
) => {
	let from,until;

	if(ms && ms.oldestThreadDate) {
		from = Moment(ms.oldestThreadDate).format(t('D/M/Y'))
		until = Moment(ms.newestThreadDate).format(t('D/M/Y'))
	}

	let threadsDates = (ms) =>
		<span style={{color:"#bbb"}}>
			{t('de')} {from} {t('a')} {until} 
		</span>

    return (
    	<div>
	    	<Header as='h3' floated="left" style={{marginBottom:"10px"}}>
	    		{t('Conversas')}
	    		<Header.Subheader>
	    			{ ms ? 
	    				<div style={{fontSize:'0.9em', marginTop:"4px"}}>
	    					{threadsDates(ms)}
	    				</div>
	    				:
	    				null
	    			}
	    		</Header.Subheader>
	    	</Header>
	    </div>
	)
}

ThreadStatus.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default compose(
	connect(
	    (state,ownProps) => ({ 
	        peerId: state.messages.peerId
	    })
	),
    graphql(
        THREADS_STATUS_QUERY,
        {
        	options: ({peerId}) => ({variables:{peerId:peerId}}),
            skip: (ownProps) => ownProps.peerId === '',
            props: ({data:{loading,error,messagesStatus}}) => {
                let ret = {
                    loading,
                    error,
                    messagesStatus,
                };
                return ret;
            }
        }
    ),
	displayLoaderAndError
)(ThreadStatus)