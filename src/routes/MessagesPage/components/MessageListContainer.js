import React from 'react'
import Help from '../components/Help'
import { compose } from 'react-apollo'
import { connect } from 'react-redux';
import messagesQueryWrapper from '../queries/messagesQueryWrapper'
import MessageList from '../components/MessageList'
import {toggleMessage} from '../ducks'
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'
import NoMessages from '../components/NoMessages'
import {branch,renderComponent} from 'recompose'

export default compose(
    connect(
        (state,ownProps) => { 
            return {
                peerId: state.messages.peerId,
                selectedMessages: state.messages.selectedMessages
            }
        },
        (dispatch) => ({
            toggleMessage(messageId) {
                dispatch(toggleMessage(messageId));
            }
        })
    ),
    messagesQueryWrapper,
    displayLoaderAndError,
    branch(
        (props)=> props.messages === undefined,
        renderComponent(()=><Help
            instructions={[
                'Escolha um canal',
                'escolha um canal na caixa acima para ler as mensens'
        ]}/>)
    ),
    branch(
        ({messages})=> messages.length===0,
        renderComponent(NoMessages)
    )
)(MessageList);