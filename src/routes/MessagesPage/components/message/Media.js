import React from 'react'
import { Image, Embed, Container } from 'semantic-ui-react'
import {withState} from 'recompose'

const withExpand = withState('expanded', 'setExpanded', false)

const Photo = withExpand(
	({threaded, collapsedThread, media, expanded, setExpanded}) => {
		let style = {}
		let onClick = null;
		if(threaded) {
			if(expanded) {
				Object.assign(style,{maxWidth: '500px'})
				onClick = (e) => {
					e.stopPropagation();
					setExpanded(false)
				}
			}
			else {
				Object.assign(style,{maxWidth: '100px', maxHeight: '80px'})	
				onClick = (e) => {
					e.stopPropagation();
					setExpanded(true)
				}
			}
		} 

		return collapsedThread ? 
			<i>image</i> :
			<Image style={style} src={media.url} onClick={onClick}/>
	}
)



const Document = ({media,collapsedThread}) => {
	return collapsedThread ? 
		<i>document</i> :
		<Embed src={media.url} />
}

const Audio = ({media, collapsedThread}) => {
	return collapsedThread ? 
		<i>audio</i> : 
		(
			<audio controls>
				<source src={media.url} type={media.mimetype}/>
			</audio>
		)
}

const Video = ({media,collapsedThread,threaded}) => {
	const style = (threaded) ? {maxWidth: '100px', maxHeight: '80px'} : {}
	return collapsedThread ? 
		<i>video</i> :
		(
			<video controls style={style}>
				<source src={media.url} type={media.mimetype}/>
			</video>
		)
}

export default ({media, threaded, collapsedThread}) => {
	let element
	switch ( media.type.name ) {
		case "Photo":
			element = <Photo {...{threaded, collapsedThread,media}}/>
		break
		case "Document":
			element = Document({media,collapsedThread})
		break
		case "Audio":
			element = Audio({collapsedThread, media})
		break
		case "Video":
			element = Video({media,collapsedThread,threaded})
		break
		default:
			element = <span> MediaType desconhecido: {media.type.name} </span>
			console.log('MediaType desconhecido: ', media)
		break
	}
	return <Container style={{clear: 'right'}}>{element}</Container>
}