import { compose, graphql } from 'react-apollo'
import { connect } from 'react-redux';
import MessageButtons from './MessageButtons'
import MESSAGES_CLEAN_MUTATION from '../../mutations/messagesClean.graphql'
import THREADS_QUERY from '../../queries/threads.graphql'
import THREADS_STATUS_QUERY from '../../queries/threadsStatus.graphql'
import CLEAN_MESSAGES_STATUS_QUERY from '../../queries/cleanMessagesStatus.graphql'
import mutationRunner from '../../../../HOCS/mutationRunner'

export default compose(
	connect(
        (state,ownProps) => ({ 
            peerId: state.messages.peerId,
        }),
    ),
    graphql(MESSAGES_CLEAN_MUTATION, {
        props: ({ mutate, ownProps }) => ({
            cleanMessages: ( messageIds ) => mutate({
                variables: { messageIds:[messageIds] },
                refetchQueries: [
                    {
                        query: CLEAN_MESSAGES_STATUS_QUERY,
                        variables: { peerId: ownProps.peerId },
                    },
                    {
                        query: THREADS_QUERY,
                        variables: { peerId: ownProps.peerId },
                    },
                    {
                        query: THREADS_STATUS_QUERY,
                        variables: { peerId: ownProps.peerId },
                    }
                ],
            })
        })
    }),
    mutationRunner
)(MessageButtons);