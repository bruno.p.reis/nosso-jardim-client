import React from 'react'
import { Comment } from 'semantic-ui-react'

export default ({
	message,
	cleanMessages,
	showRemoveButton,
	collapsed,
	runMutation,
	mutationLoaderAndError
}) => {
	
	function clean(e) {
		e.stopPropagation();
		console.log('clean');
		runMutation(
			()=>cleanMessages(message.id)
		)
	}
	if(!collapsed) {
		let content;
		if (message.irrelevant) {
			content = (
				<Comment.Actions>
		            <Comment.Action onClick={clean}>Não Irrelevante</Comment.Action>
		        </Comment.Actions>
			)
		} else if(showRemoveButton) {
			content = (
				<Comment.Actions>
		            <Comment.Action onClick={clean}>Retirar Mensagem</Comment.Action>
		        </Comment.Actions>
			)
		}
		return (
			<span>
				{mutationLoaderAndError}
				{content}
			</span>
		)
	}
	return null;
}