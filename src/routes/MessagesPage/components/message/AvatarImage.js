import React from 'react'
import { Comment } from 'semantic-ui-react'

export default ({message,collapsedThread}) => {
	const photo = message.sender.photo
	const avatarSource = photo ? photo.url : 'images/avatars/annonymous.png'
	return <Comment.Avatar className={collapsedThread?"collapsed":null} src={avatarSource} />
}