import { compose, graphql } from 'react-apollo'
import { connect } from 'react-redux';
import displayLoaderAndError from '../../../HOCS/displayLoaderAndError'
import {clearSelection,setPeer} from '../ducks'
import PEERS_QUERY from '../queries/peers.graphql'
import MESSAGES_STATUS_QUERY from '../queries/messagesStatus.graphql'
import MessageStatus from './MessageStatus'

export default compose(
	connect(
	    (state,ownProps) => ({ 
	        selectedMessages: state.messages.selectedMessages,
	        peerId: state.messages.peerId
	    }),
	    (dispatch) => ({
	        clearSelection: () => dispatch(clearSelection()),
	        setPeer: (peerId) => dispatch(setPeer(peerId))
	    })
	),
	graphql(
        PEERS_QUERY,
        {
            props: ({data:{loading,error,peers}}) => {
                let ret = {
                    loading,
                    error,
                    peers,
                };
                return ret;
            }
        }
    ),
    graphql(
        MESSAGES_STATUS_QUERY,
        {
        	options: ({peerId}) => ({variables:{peerId:peerId}}),
            skip: (ownProps) => ownProps.peerId === '',
            props: ({data:{loading,error,messagesStatus}}) => {
                let ret = {
                    loading,
                    error,
                    messagesStatus,
                };
                return ret;
            }
        }
    ),
	displayLoaderAndError
)(MessageStatus)