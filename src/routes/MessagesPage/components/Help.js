import React from 'react'
import { Header, Icon } from 'semantic-ui-react'

export default ({instructions,hide}) => {
	if(hide) return null;
	const headerContent = instructions[0]
	const messages = instructions.slice(1)
	return (
	    <Header as='h4' style={{marginTop:"20px"}}>
			<Icon name='info' circular />
			<Header.Content>
				{headerContent}
			</Header.Content>
			<Header.Subheader style={{marginLeft:"55px",lineHeight:"1.5em"}}>
				{messages.map((m,i)=>
					<span key={i}> - {m} <br/> </span>
				)}
	    	</Header.Subheader>
	    </Header>
	)
}

