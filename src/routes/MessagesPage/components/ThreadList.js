import React from 'react'
import Thread from '../components/Thread'
import Help from '../components/Help'
export default ({
    threads,
    cleanMessages,
    addMessagesToThread,
    selectedMessagesCount
}) => 
    <div style={{width:"99%"}}>
        <Help
            hide={threads.length !== 1}
            instructions={[
                'Expandir Conversa',
                'as conversas são listadas comprimidas',
                'clique em uma conversa para expandi-la'
        ]}/>
        <Help
            hide={threads.length !== 1}
            instructions={[
                'Remover Mensagem',
                'expanda a conversa',
                'clique em "Retirar Mensagem"'
        ]}/>
        <Help
            hide={threads.length !== 2}
            instructions={[
                'Lista de Mensagens',
                'a lista de mensagens inicia da primeira mensagem a ser catalogada',
                'clique em "...anteriores" para ver mensagens já catalogadas',
                'clique em "próximas..." para carregar mais'
        ]}/>
        <Help
            hide={threads.length !== 2}
            instructions={[
                'Mensagens Irrelevantes',
                'mensagens cujo conteúdo não precisa ser catalogado',
                'selecione uma ou mais mensagem e clique em "Irrelevante"'
        ]}/>
        {threads.map( (thread) =>
            <Thread
                thread={thread}
                cleanMessages={cleanMessages}
                addMessagesToThread={ (e)=>{
                    e.stopPropagation();
                    addMessagesToThread(thread.id)
                } }
                selectedMessagesCount={selectedMessagesCount}
                key={thread.id}/>
        )}
    </div>