import React, { Component } from 'react'
import { Segment } from 'semantic-ui-react'
import classNames from 'classnames'
import ThreadMessages from './threads/ThreadMessages'
import ThreadHeader from './threads/ThreadHeader'

class Thread extends Component {
    constructor(props) {
        super(props)
        this.state = { collapsed: true }
    }

    toggleShowMessages = (e) => {
        e.stopPropagation()
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render = () => {
        const{
            cleanMessages,
            thread,
            selectedMessagesCount,
            addMessagesToThread
        } = this.props;
        const {t} = this.context;

        const collapsed = this.state.collapsed; 
        
        const footer = () => {
            if(collapsed && thread.messages.length > 2) {
                const messageAuthors = thread.messages.map( (message) => message.sender.name )
                const uniqueAuthors = [...new Set(messageAuthors)].join(', ')
                    .replace(/,([^,]*)$/, ' '+t('e')+'$1') // 'e' no lugar da última vírgula

                return (
                    <span style={{fontSize:"0.8em", color: '#999'}}>
                        &nbsp;&nbsp;&nbsp;
                        {uniqueAuthors}
                    </span>
                )
            }
            else {
                return null
            }
        }

        const threadClass = classNames({
            thread: true,
            collapsed: true,
        })
        const mediaTypes = thread.messages
            .filter( (message) => message.media )
            .map( (message) => message.media.type.name )
        return (
            <Segment  
                className={threadClass} 
                onClick={this.toggleShowMessages} 
                style={{
                    cursor: (collapsed ? "row-resize" : "initial"), 
                    backgroundColor: collapsed ? "#f5f5f5" : "#fff",
                    padding: "10px 10px 5px 10px"
                }}>
                <ThreadHeader
                    collapsed={collapsed}
                    addMessagesToThread={addMessagesToThread}
                    toggleMessages={this.toggleShowMessages}
                    selectedMessagesCount={selectedMessagesCount}
                    mediaTypes={mediaTypes}
                    thread={thread}/>
                <br style={{clear:"both"}}/>
                <ThreadMessages
                    cleanMessages={cleanMessages}
                    messages={thread.messages}
                    collapsed={collapsed}/>
                {footer()}
            </Segment>
        )
    }
}

Thread.contextTypes = {
  t: React.PropTypes.func.isRequired
}

export default Thread
