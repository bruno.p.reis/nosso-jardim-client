import React, { Component } from 'react'
import { Grid, Container } from 'semantic-ui-react'
import {connect} from 'react-redux'
import MessageListContainer from './components/MessageListContainer'
import ThreadListContainer from './components/ThreadListContainer'
import MessageStatusContainer from './components/MessageStatusContainer'
import ThreadStatusContainer from './components/ThreadStatusContainer'

class MessagesPage extends Component {
    render() {
        return (
            <Grid padded={true} columns='two' style={{height:'100%'}}>
                <Grid.Row>
                    <Grid.Column>
                        <MessageStatusContainer/>
                        <Container style={{overflow:"auto", height:"400px"}}>
                            <MessageListContainer/>
                        </Container>
                    </Grid.Column>
                    <Grid.Column>
                        <ThreadStatusContainer/>
                        <Container  style={{overflow:"auto", height:"500px"}}>
                            <ThreadListContainer/>
                        </Container>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default MessagesPage
