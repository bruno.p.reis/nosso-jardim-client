import update from 'immutability-helper';
import _ from 'lodash'
const CLEAR_SELECTION = 'nossoJardim/messages/CLEAR_SELECTION';
const SET_PEER = 'nossoJardim/messages/SET_PEER';
const TOGGLE_MESSAGE = 'nossoJardim/messages/TOGGLE_MESSAGE';

export function clearSelection() {
    return {type: CLEAR_SELECTION};
}

export function setPeer(peerId) {
    return {
        type: SET_PEER,
        peerId:peerId
    };
}

export function toggleMessage(messageId, includeItself = false) {
    return {
        type: TOGGLE_MESSAGE,
        messageId: messageId
    };
}


// ### ### ### Reducer

function subtopicsReducer(state = {selectedMessages: [],peerId: '2'}, action, apolloState) {
    switch (action.type) {
        case SET_PEER:
            let newState = Object.assign({},state,{peerId:action.peerId})
            return newState;
        case CLEAR_SELECTION:
            return Object.assign({},state,{selectedMessages:[]})
        case TOGGLE_MESSAGE:
            const indexOfId = state.selectedMessages.indexOf(action.messageId);
            const selected = (indexOfId === -1) ? {$push: [action.messageId]} : {$splice: [[indexOfId,1]]}
            return update( state, {selectedMessages:selected} )
        default:
          return state;
    }
}
export default subtopicsReducer;