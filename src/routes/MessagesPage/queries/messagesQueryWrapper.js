import MESSAGES_QUERY from './messages.graphql'
import PREVIOUS_MESSAGES_QUERY from '../queries/previousMessages.graphql'
import { graphql } from 'react-apollo'
export default graphql(
    MESSAGES_QUERY,
    {
        options: ({peerId}) => ({variables:{peerId:peerId}}),
        skip: (ownProps) => ownProps.peerId === '',
        props: ({
            ownProps:{
                peerId
            },
            data:{
                loading,
                messages,
                fetchMore,
                error,
                refetch
            }
        }) => {
            return {
                loading,
                error,
                refetchMessages:refetch,
                messages: messages ? messages.edges.map((e)=>e.node) : [],
                hasPreviousPage: messages ? messages.pageInfo.hasPreviousPage : null,
                hasNextPage: messages ? messages.pageInfo.hasNextPage : null,
                loadMoreEntries: () => {
                    return fetchMore({
                        query: MESSAGES_QUERY,
                        variables: {
                            cursor: messages.pageInfo.endCursor,
                            peerId: peerId
                        },
                        updateQuery: (previousResult, { fetchMoreResult }) => {
                            const newEdges = fetchMoreResult.messages.edges;
                            const receivedPI = fetchMoreResult.messages.pageInfo;
                            const pageInfo = {
                                ...previousResult.messages.pageInfo,
                                endCursor: receivedPI.endCursor,
                                hasNextPage: receivedPI.hasNextPage
                            }

                            return {
                                messages: {
                                    edges: [...previousResult.messages.edges, ...newEdges],
                                    pageInfo,
                                },
                            };
                        },
                    });
                },
                loadPreviousEntries: () => {
                    return fetchMore({
                        query: PREVIOUS_MESSAGES_QUERY,
                        variables: {
                            cursor: messages.pageInfo.startCursor,
                            peerId: peerId
                        },
                        updateQuery: (previousResult, { fetchMoreResult }) => {
                            const newEdges = fetchMoreResult.messages.edges;
                            const receivedPI = fetchMoreResult.messages.pageInfo;
                            const pageInfo = {
                                ...previousResult.messages.pageInfo,
                                startCursor: receivedPI.startCursor,
                                hasPreviousPage: receivedPI.hasPreviousPage
                            }

                            return {
                                messages: {
                                    edges: [...newEdges,...previousResult.messages.edges],
                                    pageInfo,
                                },
                            };
                        },
                    });
                }
            };
        }
    }
)