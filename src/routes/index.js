import React from 'react'
import { Route, IndexRoute } from 'react-router'

import Layout from '../Layouts/Layout'
import MessagesPage from './MessagesPage/MessagesPage'
import ItemsPage from './ItemsPage/ItemsPage'
import SubtopicsPage from './SubtopicsPage/SubtopicsPage'

export default (
    <Route path="/" component={Layout}>
        <IndexRoute component={MessagesPage}/>
        <Route path="messages" component={MessagesPage}/>
        <Route path="items" component={ItemsPage} />
        <Route path="items/create" component={ItemsPage} create={true}/>
        <Route path="items(/subtopic/:subtopicId)" component={ItemsPage}/>
        <Route path="items(/subtopic/:subtopicId/createItem)" component={ItemsPage} createItem={true}/>
        <Route path="items(/subtopic/:subtopicId/item/:itemId/edit)" component={ItemsPage} editItem={true}/>
        <Route path="subtopics" component={SubtopicsPage}/>
        <Route path="subtopics/create" component={SubtopicsPage} create={true}/>
        <Route path="subtopics(/:subtopicId)/create" component={SubtopicsPage} create={true}/>
        <Route path="subtopics(/:subtopicId)" component={SubtopicsPage} edit={true} />
    </Route>
);
