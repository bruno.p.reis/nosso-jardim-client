import React from 'react'
import { Label, Form, Segment, Header } from 'semantic-ui-react'
import _ from 'lodash'
export default (props) => {
	
	const {
		title,
		children,
		close,
		style,
		onSubmit
	} = props;

	const omit = [
		'title',
		'children',
		'close',
		'style',
		'onSubmit'
	];

	return (
		<div {..._.omit(props,omit)} style={{position: 'relative',...style}}>
	        <Header block as='h4' attached='top'>
	            {title}
	            <Label
	                style={{marginTop: 1}}
	                size='mini'
	                attached='top right'
	                as='a'
	                onClick={(e)=>{
	                	e.stopPropagation();
	                	if(close){
	                		close()
	                	}
	                }}
	                icon={{ size: 'large', name: 'remove', style: {margin: 0} }}/>
	        </Header>
	        <Segment attached>
	            <Form {...{onSubmit}}>
	                {children}
	            </Form>
	        </Segment>
	    </div>
	);
}