import React from 'react'
import { withState, pure, compose, branch, renderComponent } from 'recompose';
import { Button } from 'semantic-ui-react'


const NormalState = ({icon,setConfirmationRequested}) => 
	<Button
        icon={icon}
        onClick={()=>setConfirmationRequested(true)}/>

const ConfirmButtonPure = ({executeAction,icon,confirmationRequested,setConfirmationRequested}) => 
	<span onMouseLeave={()=>setConfirmationRequested(false)}>
		<Button compact
			icon='checkmark'
			positive
			onClick={executeAction}
			attached='left'/>
		<Button compact
			icon='remove'
			onClick={()=>setConfirmationRequested(false)}
			attached='right'/>
	</span>

const displayNormalState = branch(
	(props) => !props.confirmationRequested,
	renderComponent(NormalState)
)

const confirmationRequested = withState('confirmationRequested','setConfirmationRequested',false);

export default compose(
	confirmationRequested,
	displayNormalState,
	pure
)(ConfirmButtonPure);