import React, {PropTypes} from 'react'
import { Button, Header, Dimmer, Loader } from 'semantic-ui-react'

const LoaderAndError = ({loading,errorMessage,dismissFunc}) => {
	const active = !!(loading || errorMessage)
	if(errorMessage) {
		if(errorMessage.message) {
			errorMessage = errorMessage.message
		}
		errorMessage = errorMessage.replace('GraphQL error: ','')
	}
	let dismissButton;
	if(dismissFunc) {
		dismissButton = <Button size='tiny' onClick={dismissFunc}>ok</Button>
	}
	return (
		<Dimmer inverted active={active}>
			{
				errorMessage ? 
                <Header as='h3'>
                    {errorMessage}
                </Header> :
                null
			}
			{
				loading ?
            	<Loader/> :
            	null
			}
			{   
				dismissFunc && errorMessage ? 
				dismissButton :
				''
			}
        </Dimmer>
	)	
}			

LoaderAndError.propTypes = {
    loading: PropTypes.bool.isRequired,
    errorMessage: PropTypes.any
}

export default LoaderAndError