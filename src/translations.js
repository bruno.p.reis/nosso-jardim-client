export const translations = {
  "en": {
    "Mensagens": "Messages",
    "Conversas": "Threads",
    "Desselecionar": "Clear Selection",
    "Marcar como Irrelevantes": "Mark as Irrelevant",
    "Criar Nova Conversa": "Create Thread",
    "Subtópicos": "Sutopics",
    "Items": "Itens",
    "de":"from",
    "a":"to",
    "e":"and",
    "D/M/Y":"M/D/Y",
    "anteriores":"previous",
    "próximas":"next"
  }
}