import merge from "lodash/merge"
import defaults from "./default.js"
import local from "./local.js"

let config = require("./" + (process.env.NODE_ENV || "development") + ".js")

module.exports = merge({}, defaults, config, local);
