import React, { Component } from 'react';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Router, browserHistory } from 'react-router';
import config from './config/index.js';
import '../node_modules/semantic-ui-css/semantic.min.css';
import { ApolloProvider } from 'react-apollo';
import ApolloClient, { toIdValue,createNetworkInterface } from 'apollo-client';

import subtopicsReducer from './routes/SubtopicsPage/ducks.js'
import messagesReducer from './routes/MessagesPage/ducks.js'

import routes from './routes'

import I18n from "redux-i18n"
import {translations} from "./translations"

import {i18nState} from "redux-i18n"

const networkInterface = createNetworkInterface({
    uri: config.endpoints.threads
});

const dataIdFromObject = (result) => {
    return result.id && result.__typename ?
        result.__typename + '_' + result.id : 
        null
}

const customResolvers = {
    Query: {
        subtopic: (_, args) => {
            return toIdValue(
                dataIdFromObject({ 
                    __typename: 'Subtopic', 
                    id: args['id'] 
                })
            )
        },
        item: (_, args) => {
            return toIdValue(
                dataIdFromObject({ 
                    __typename: 'Item', 
                    id: args['id'] 
                })
            )
        }
    }
}

const client = new ApolloClient({
    networkInterface,
    customResolvers,
    dataIdFromObject 
});

const store = createStore(
    combineReducers({
        apollo: client.reducer(),
        subtopics: subtopicsReducer,
        messages: messagesReducer,
        i18nState
    }),
    {}, // initial state
    compose(
        applyMiddleware(client.middleware()),
        // If you are using the devToolsExtension, you can add it here also
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    )
);

class App extends Component {


    render() {
        return (
                <ApolloProvider store={store} client={client}>
                    <I18n translations={translations} initialLang="pt-br">
                		<Router history={browserHistory}>
        	        		{routes}
                		</Router>
                    </I18n>
                </ApolloProvider>
        );
    }
}

export default App
