### Pré-requisitos
- O [servidor](https://gitlab.com/bruno.p.reis/nosso-jardim) deve ser rodado ao mesmo tempo que o cliente

### Instalação
```sh
git clone https://gitlab.com/bruno.p.reis/nosso-jardim-client.git
cd nosso-jardim-client
npm install
```

### Configuração
- Copie o arquivo src/config/local.default.js para src/config/local.js
- Altere o endpoint do servidor no arquivo src/config/local.js

### Utilização
```sh
npm start
```
### Desenvolvimento
```
.
|____public
|____src
  |____config
  |____routes
    |____Layouts
    |____Messages
    | |____components
    | |____containers
    |____Threads
      |____components
      |____containers
```
O arquivo src/App.js distribui a interface em rotas pelo react-router, que são configuradas em src/routes/index.js. O layout principal fica em src/routes/Layouts/Layout.js. Componentes funcionais (Messages e Threads) estão divididos em ./components (componentes de apresentação) e ./containers (componentes containers), e ficam em diretórios próprios.

### Documentação dos principais módulos utilizados
- [react](https://facebook.github.io/react/docs/hello-world.html)
- [apollo](http://dev.apollodata.com/react/)
- [graphql](http://graphql.org/learn/)
- [react-router](https://github.com/reactjs/react-router-tutorial)
- [create-react-app](https://github.com/facebookincubator/create-react-app)
- [react-semantic-ui](http://react.semantic-ui.com/introduction)

### Referências
- [An example app frontend built with Apollo Client and React](https://github.com/apollostack/GitHunt-React)
- [Apollo React example for Github GraphQL API](https://github.com/katopz/react-apollo-graphql-github-example)


### Colocar loader para componente que dispara mutation: 


